#!/bin/bash

source lib.sh

if [ ! -z "$1" ]; then

while getopts "fh" Option; do
	case $Option in
		f | --file)
		echo "Search results of $3 in file: $2"
		readfile "$2" "$3";;

		h | --help)
		help; exit 0;;

		*)
		echo "Wrong parameter" 1>&2; exit 1;;

	esac
	done
	shift $((OPTIND - 1))
exit 0 

else
	help; exit 0
fi
