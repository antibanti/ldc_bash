#!/bin/bash

function help {
	echo -e 'A simple script for searching words in file and counting matches.\n./script.sh [file]... [search word]...\n-f - File.\n-h - help and exit.'
	exit 1
}

function count_str {
	wc -l <&0
}

function readfile {
	cat "$1" | grep -vE "(^#|^$)" | grep "$2" | count_str 
}
