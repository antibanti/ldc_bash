#!/bin/bash

if [[ "$1" = '-fl' || "$1" = '-lf' || "$1" = '--log' || "$1" = '-f' || "$1" = '--file' || "$1" = '-l' ]]; then
	while getopts ":fl" Option
	do 
	case $Option in

		f | -file) 
		if [ -z "$2" ] 
		then
		echo "Please write <filename> after flag $1" 1>&2
		else
		echo "" > "$2"
		fi;;
		
		l | -log)
		if [ -z "$3" ]
		then
		echo "Please write your text after <filename>" 1>&2
		else
		echo "$(date +"%T") $3" > "$2"
		fi;;
	
		#*) echo "Wrong key";;
 	
	esac
	done
	shift $((OPTIND - 1))
	exit 0

elif [[ "$1" = '-fla' || "$1" = '-lfa' ]]; then
	while getopts ":fla" Option
	do 
		case $Option in

		f | -file) 
		if [ -z "$2" ] 
		then
		echo "Please write <filename> after flag $1" 1>&2
		else
		touch "$2"
		fi;;
		
		l | --log)
		if [ -z "$3" ]
		then
		echo "Please write your text after <filename>" 1>&2
		else
		log="$(date +"%T") $3"
		echo $log >> "$2"
		fi;;
		
		a | --append);;

		*) echo "Wrong key";;
 	
	esac
	done
	shift $((OPTIND - 1))
	exit 0	

elif [[ "$1" = '-h' || "$1" = '--help' ]]; then
	echo "Full description of the script"; exit 0

else
	echo "Please use -h or --help to find some useful information" 1>&2; exit 1
	
fi	