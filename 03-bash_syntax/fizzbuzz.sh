#!/bin/bash

for i in {1..100}; do { (( $i % 3 == 0 && $i % 5 == 0 )) && echo "FizzBuzz"; } || { (( $i % 3 == 0 )) && echo "Fizz"; } || { (( $i % 5 == 0 )) && echo "Buzz"; } || echo $i; done